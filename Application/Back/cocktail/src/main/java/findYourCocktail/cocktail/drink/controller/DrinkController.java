package findYourCocktail.cocktail.drink.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import findYourCocktail.cocktail.drink.bean.Drink;
import findYourCocktail.cocktail.drink.service.DrinkService;
import findYourCocktail.cocktail.exception.DrinkImplException;

/**
 * Couche controller de Drink
 * @author Pirathina Jeyakumar
 *
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/drinks")
public class DrinkController {

	@Autowired
	private DrinkService drinkService;
	
	/**
	 * Controller pour afficher tous les drinks
	 * @return liste de tous les drinks
	 * @throws Exception
	 */
	@GetMapping(path = "/tousLesDrinks")
	public List<Drink> afficherTousLesDrinks() throws Exception{
		return drinkService.afficherTousLesDrinks();
	}
	
	/**
	 * Controller pour afficher les drinks selon la lettre entrée en paramètre
	 * @param lettre
	 * @return liste des drinks selon la lettre entrée en paramètre
	 * @throws Exception
	 */
	@GetMapping(path = "/drinksParPremiereLettre/{lettre}")
	public List<Drink> afficherDrinksParPremiereLettre(@PathVariable String lettre) throws Exception {
		return drinkService.afficherDrinksParPremiereLettre(lettre);
	}
	
	/**
	 * Controller pour afficher les drinks selon le nom entré en paramètre
	 * @param nom
	 * @return liste des drinks selon le nom entré en paramètre
	 * @throws Exception
	 */
	@GetMapping(path = "/drinksParNom/{nom}")
	public List<Drink> afficherDrinkParNom(@PathVariable String nom) throws Exception {
		return drinkService.afficherDrinkParNom(nom);
	}
	
	/**
	 * Controller pour afficher le drink associé à l'id entré en paramètre
	 * @param id
	 * @return drink associé à l'id entré en paramètre
	 * @throws Exception
	 */
	@GetMapping(path = "/drinkParId/{id}")
	public List<Drink> afficherDrinkParId(@PathVariable int id) throws Exception{
		return drinkService.afficherDrinkParId(id);
	}
	
	/**
	 * Controller pour afficher un drink de facon aléatoire
	 * @return drink aléatoire
	 * @throws Exception
	 */
	@GetMapping(path = "/drinkRandom")
	public List<Drink> afficherUnDrinkRandom() throws Exception{
		return drinkService.afficherUnDrinkRandom();
	}
	
	/**
	 * Controller pour afficher les drinks associés à la catégorie entrée en paramètre
	 * @param categorie
	 * @return liste des drinks selon la catégorie entré en paramètre
	 * @throws IOException
	 * @throws DrinkImplException
	 */
	@GetMapping(path = "/drinksParCategorie/{categorie}")
	public List<Drink> afficherDrinksParCategorie(@PathVariable String categorie) throws IOException, DrinkImplException{
		return drinkService.afficherDrinksParCategorie(categorie);
	}
}
