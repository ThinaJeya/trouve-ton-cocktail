package findYourCocktail.cocktail.drink.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Couche Bean de Drink
 * @author Pirathina Jeyakumar
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class Drink {

	/**
	 * id du drink
	 */
	@Id
	private String idDrink;

	/**
	 * Nom du drink
	 */
	private String strDrink;

	/**
	 * Catégorie du drink
	 */
	private String strCategory;

	/**
	 * Précise si le drink est alcoolisé ou non
	 */
	private String strAlcoholic;

	/**
	 * Nom du verre à utiliser pour le drink
	 */
	private String strGlass;

	/**
	 * Instructions de préparation du drink
	 */
	private String strInstructions;

	/**
	 * Lien de l'image du drink
	 */
	private String strDrinkThumb;

	/**
	 * Nom de l'ingrédient 1 pour la préparation du drink
	 */
	private String strIngredient1;

	/**
	 * Nom de l'ingrédient 2 pour la préparation du drink
	 */
	private String strIngredient2;

	/**
	 * Nom de l'ingrédient 3 pour la préparation du drink
	 */
	private String strIngredient3;

	/**
	 * Nom de l'ingrédient 4 pour la préparation du drink
	 */
	private String strIngredient4;

	/**
	 * Nom de l'ingrédient 5 pour la préparation du drink
	 */
	private String strIngredient5;

	/**
	 * Nom de l'ingrédient 6 pour la préparation du drink
	 */
	private String strIngredient6;

	/**
	 * Nom de l'ingrédient 7 pour la préparation du drink
	 */
	private String strIngredient7;

	/**
	 * Nom de l'ingrédient 8 pour la préparation du drink
	 */
	private String strIngredient8;

	/**
	 * Nom de l'ingrédient 9 pour la préparation du drink
	 */
	private String strIngredient9;

	/**
	 * Nom de l'ingrédient 10 pour la préparation du drink
	 */
	private String strIngredient10;

	/**
	 * Nom de l'ingrédient 11 pour la préparation du drink
	 */
	private String strIngredient11;

	/**
	 * Nom de l'ingrédient 12 pour la préparation du drink
	 */
	private String strIngredient12;

	/**
	 * Nom de l'ingrédient 13 pour la préparation du drink
	 */
	private String strIngredient13;

	/**
	 * Nom de l'ingrédient 14 pour la préparation du drink
	 */
	private String strIngredient14;

	/**
	 * Nom de l'ingrédient 15 pour la préparation du drink
	 */
	private String strIngredient15;

	/**
	 * Dose de l'ingrédient 1
	 */
	private String strMeasure1;

	/**
	 * Dose de l'ingrédient 2
	 */
	private String strMeasure2;

	/**
	 * Dose de l'ingrédient 3
	 */
	private String strMeasure3;

	/**
	 * Dose de l'ingrédient 4
	 */
	private String strMeasure4;

	/**
	 * Dose de l'ingrédient 5
	 */
	private String strMeasure5;

	/**
	 * Dose de l'ingrédient 6
	 */
	private String strMeasure6;

	/**
	 * Dose de l'ingrédient 7
	 */
	private String strMeasure7;

	/**
	 * Dose de l'ingrédient 8
	 */
	private String strMeasure8;

	/**
	 * Dose de l'ingrédient 9
	 */
	private String strMeasure9;

	/**
	 * Dose de l'ingrédient 10
	 */
	private String strMeasure10;

	/**
	 * Dose de l'ingrédient 11
	 */
	private String strMeasure11;

	/**
	 * Dose de l'ingrédient 12
	 */
	private String strMeasure12;

	/**
	 * Dose de l'ingrédient 13
	 */
	private String strMeasure13;

	/**
	 * Dose de l'ingrédient 14
	 */
	private String strMeasure14;

	/**
	 * Dose de l'ingrédient 15
	 */
	private String strMeasure15;

	/**
	 * Date de dernière modification du drink
	 */
	private String dateModified;

	public Drink(String idDrink, String strDrink, String strCategory,
			String strAlcoholic, String strGlass, String strInstructions, String strDrinkThumb, String strIngredient1,
			String strIngredient2, String strIngredient3, String strIngredient4, String strIngredient5,
			String strIngredient6, String strIngredient7, String strIngredient8, String strIngredient9,
			String strIngredient10, String strIngredient11, String strIngredient12, String strIngredient13,
			String strIngredient14, String strIngredient15, String strMeasure1, String strMeasure2, String strMeasure3,
			String strMeasure4, String strMeasure5, String strMeasure6, String strMeasure7, String strMeasure8,
			String strMeasure9, String strMeasure10, String strMeasure11, String strMeasure12, String strMeasure13,
			String strMeasure14, String strMeasure15, String dateModified) {
		super();
		this.idDrink = idDrink;
		this.strDrink = strDrink;
		this.strCategory = strCategory;
		this.strAlcoholic = strAlcoholic;
		this.strGlass = strGlass;
		this.strInstructions = strInstructions;
		this.strDrinkThumb = strDrinkThumb;
		this.strIngredient1 = strIngredient1;
		this.strIngredient2 = strIngredient2;
		this.strIngredient3 = strIngredient3;
		this.strIngredient4 = strIngredient4;
		this.strIngredient5 = strIngredient5;
		this.strIngredient6 = strIngredient6;
		this.strIngredient7 = strIngredient7;
		this.strIngredient8 = strIngredient8;
		this.strIngredient9 = strIngredient9;
		this.strIngredient10 = strIngredient10;
		this.strIngredient11 = strIngredient11;
		this.strIngredient12 = strIngredient12;
		this.strIngredient13 = strIngredient13;
		this.strIngredient14 = strIngredient14;
		this.strIngredient15 = strIngredient15;
		this.strMeasure1 = strMeasure1;
		this.strMeasure2 = strMeasure2;
		this.strMeasure3 = strMeasure3;
		this.strMeasure4 = strMeasure4;
		this.strMeasure5 = strMeasure5;
		this.strMeasure6 = strMeasure6;
		this.strMeasure7 = strMeasure7;
		this.strMeasure8 = strMeasure8;
		this.strMeasure9 = strMeasure9;
		this.strMeasure10 = strMeasure10;
		this.strMeasure11 = strMeasure11;
		this.strMeasure12 = strMeasure12;
		this.strMeasure13 = strMeasure13;
		this.strMeasure14 = strMeasure14;
		this.strMeasure15 = strMeasure15;
		this.dateModified = dateModified;
	}

	public Drink() {

	}

	public Drink(String idDrink, String strDrink) {
		this.idDrink = idDrink;
		this.strDrink = strDrink;
	}
	
	public Drink(String idDrink, String strDrink, String strDrinkThumb) {
		super();
		this.idDrink = idDrink;
		this.strDrink = strDrink;
		this.strDrinkThumb = strDrinkThumb;
	}

	/**
	 * Permet d'obtenir l'id du drink
	 * @return id du drink

	 */
	public String getIdDrink() {
		return idDrink;
	}

	/**
	 * Permet de modifier l'id du drink
	 * @param idDrink
	 */
	public void setIdDrink(String idDrink) {
		this.idDrink = idDrink;
	}

	/**
	 * Permet d'obtenir le nom du drink
	 * @return nom du drink
	 */
	public String getStrDrink() {
		return strDrink;
	}

	/**
	 * Permet de modifier le nom du drink
	 * @param strDrink
	 */
	public void setStrDrink(String strDrink) {
		this.strDrink = strDrink;
	}

	/**
	 * Permet d'obtenir la catégorie du drink
	 * @return Catégorie du drink
	 */
	public String getStrCategory() {
		return strCategory;
	}

	/**
	 * Permet de modifier la catégorie du drink
	 * @param strCategory
	 */
	public void setStrCategory(String strCategory) {
		this.strCategory = strCategory;
	}

	/**
	 * Permet de savoir si le drink est alcoolisé ou non, ou si l'alcool est facultatif
	 * @return type de drink (alcoolisé, non alcoolisé, alcool optionel)
	 */
	public String getStrAlcoholic() {
		return strAlcoholic;
	}

	/**
	 * Permet de modifier le type du drink (alcoolisé, non alcoolisé, alcool optionel)
	 * @param strAlcoholic
	 */
	public void setStrAlcoholic(String strAlcoholic) {
		this.strAlcoholic = strAlcoholic;
	}

	/**
	 * Permet d'obtenir le type de verre à utiliser pour préparer le drink
	 * @return type de verre
	 */
	public String getStrGlass() {
		return strGlass;
	}

	/**
	 * Permet de modifier le type de verre à utiliser pour préparer le drink
	 * @param strGlass
	 */
	public void setStrGlass(String strGlass) {
		this.strGlass = strGlass;
	}

	/**
	 * Permet d'obtenir les instructions (en) de préparation du drink
	 * @return instructions de préparation
	 */
	public String getStrInstructions() {
		return strInstructions;
	}

	/**
	 * Permet de modifier les instructions (en) de préparation du drink
	 * @param strInstructions
	 */
	public void setStrInstructions(String strInstructions) {
		this.strInstructions = strInstructions;
	}

	/**
	 * Permet d'obtenir le lien url vers l'image du drink
	 * @return url de l'image du drink
	 */
	public String getStrDrinkThumb() {
		return strDrinkThumb;
	}

	/**
	 * Permet de modifier le lien url vers l'image du drink
	 * @param strDrinkThumb
	 */
	public void setStrDrinkThumb(String strDrinkThumb) {
		this.strDrinkThumb = strDrinkThumb;
	}

	/**
	 * Permet d'obtenir le nom de l'ingrédient 1
	 * @return ingrédient 1
	 */
	public String getStrIngredient1() {
		return strIngredient1;
	}

	/**
	 * Permet de modifier le nom de l'ingrédient 1
	 * @param strIngredient1
	 */
	public void setStrIngredient1(String strIngredient1) {
		this.strIngredient1 = strIngredient1;
	}

	/**
	 * Permet d'obtenir le nom de l'ingrédient 2
	 * @return ingrédient 2
	 */
	public String getStrIngredient2() {
		return strIngredient2;
	}

	/**
	 * Permet de modifier le nom de l'ingrédient 2
	 * @param strIngredient2
	 */
	public void setStrIngredient2(String strIngredient2) {
		this.strIngredient2 = strIngredient2;
	}

	/**
	 * Permet d'obtenir le nom de l'ingrédient 3
	 * @return ingrédient 3
	 */
	public String getStrIngredient3() {
		return strIngredient3;
	}

	/**
	 * Permet de modifier le nom de l'ingrédient 3
	 * @param strIngredient3
	 */
	public void setStrIngredient3(String strIngredient3) {
		this.strIngredient3 = strIngredient3;
	}

	/**
	 * Permet d'obtenir le nom de l'ingrédient 4
	 * @return ingrédient 4
	 */
	public String getStrIngredient4() {
		return strIngredient4;
	}

	/**
	 * Permet de modifier le nom de l'ingrédient 4
	 * @param strIngredient4
	 */
	public void setStrIngredient4(String strIngredient4) {
		this.strIngredient4 = strIngredient4;
	}

	/**
	 * Permet d'obtenir le nom de l'ingrédient 5
	 * @return ingrédient 5
	 */
	public String getStrIngredient5() {
		return strIngredient5;
	}

	/**
	 * Permet de modifier le nom de l'ingrédient 5
	 * @param strIngredient5
	 */
	public void setStrIngredient5(String strIngredient5) {
		this.strIngredient5 = strIngredient5;
	}

	/**
	 * Permet d'obtenir le nom de l'ingrédient 6
	 * @return ingrédient 6
	 */
	public String getStrIngredient6() {
		return strIngredient6;
	}

	/**
	 * Permet de modifier le nom de l'ingrédient 6
	 * @param strIngredient6
	 */
	public void setStrIngredient6(String strIngredient6) {
		this.strIngredient6 = strIngredient6;
	}

	/**
	 * Permet d'obtenir le nom de l'ingrédient 7
	 * @return ingrédient 7
	 */
	public String getStrIngredient7() {
		return strIngredient7;
	}

	/**
	 * Permet de modifier le nom de l'ingrédient 7
	 * @param strIngredient7
	 */
	public void setStrIngredient7(String strIngredient7) {
		this.strIngredient7 = strIngredient7;
	}

	/**
	 * Permet d'obtenir le nom de l'ingrédient 8
	 * @return ingrédient 8
	 */
	public String getStrIngredient8() {
		return strIngredient8;
	}

	/**
	 * Permet de modifier le nom de l'ingrédient 8
	 * @param strIngredient8
	 */
	public void setStrIngredient8(String strIngredient8) {
		this.strIngredient8 = strIngredient8;
	}

	/**
	 * Permet d'obtenir le nom de l'ingrédient 9
	 * @return ingrédient 9
	 */
	public String getStrIngredient9() {
		return strIngredient9;
	}

	/**
	 * Permet de modifier le nom de l'ingrédient 9
	 * @param strIngredient9
	 */
	public void setStrIngredient9(String strIngredient9) {
		this.strIngredient9 = strIngredient9;
	}

	/**
	 * Permet d'obtenir le nom de l'ingrédient 10
	 * @return ingrédient 10
	 */
	public String getStrIngredient10() {
		return strIngredient10;
	}

	/**
	 * Permet de modifier le nom de l'ingrédient 10
	 * @param strIngredient10
	 */
	public void setStrIngredient10(String strIngredient10) {
		this.strIngredient10 = strIngredient10;
	}

	/**
	 * Permet d'obtenir le nom de l'ingrédient 11
	 * @return ingrédient 11
	 */
	public String getStrIngredient11() {
		return strIngredient11;
	}

	/**
	 * Permet de modifier le nom de l'ingrédient 11
	 * @param strIngredient11
	 */
	public void setStrIngredient11(String strIngredient11) {
		this.strIngredient11 = strIngredient11;
	}

	/**
	 * Permet d'obtenir le nom de l'ingrédient 12
	 * @return ingrédient 12
	 */
	public String getStrIngredient12() {
		return strIngredient12;
	}

	/**
	 * Permet de modifier le nom de l'ingrédient 12
	 * @param strIngredient12
	 */
	public void setStrIngredient12(String strIngredient12) {
		this.strIngredient12 = strIngredient12;
	}

	/**
	 * Permet d'obtenir le nom de l'ingrédient 13
	 * @return ingrédient 13
	 */
	public String getStrIngredient13() {
		return strIngredient13;
	}

	/**
	 * Permet de modifier le nom de l'ingrédient 13
	 * @param strIngredient13
	 */
	public void setStrIngredient13(String strIngredient13) {
		this.strIngredient13 = strIngredient13;
	}

	/**
	 * Permet d'obtenir le nom de l'ingrédient 14
	 * @return ingrédient 14
	 */
	public String getStrIngredient14() {
		return strIngredient14;
	}

	/**
	 * Permet de modifier le nom de l'ingrédient 14
	 * @param strIngredient14
	 */
	public void setStrIngredient14(String strIngredient14) {
		this.strIngredient14 = strIngredient14;
	}

	/**
	 * Permet d'obtenir le nom de l'ingrédient 15
	 * @return ingrédient 15
	 */
	public String getStrIngredient15() {
		return strIngredient15;
	}

	/**
	 * Permet de modifier le nom de l'ingrédient 15
	 * @param strIngredient15
	 */
	public void setStrIngredient15(String strIngredient15) {
		this.strIngredient15 = strIngredient15;
	}

	/**
	 * Permet d'obtenir la dose de l'ingrédient 1
	 * @return dose de l'ingrédient 1
	 */
	public String getStrMeasure1() {
		return strMeasure1;
	}

	/**
	 * Permet de modifier la dose de l'ingrédient 1
	 * @param strMeasure1
	 */
	public void setStrMeasure1(String strMeasure1) {
		this.strMeasure1 = strMeasure1;
	}

	/**
	 * Permet d'obtenir la dose de l'ingrédient 2
	 * @return dose de l'ingrédient 2
	 */
	public String getStrMeasure2() {
		return strMeasure2;
	}

	/**
	 * Permet de modifier la dose de l'ingrédient 2
	 * @param strMeasure2
	 */
	public void setStrMeasure2(String strMeasure2) {
		this.strMeasure2 = strMeasure2;
	}

	/**
	 * Permet d'obtenir la dose de l'ingrédient 3
	 * @return dose de l'ingrédient 3
	 */
	public String getStrMeasure3() {
		return strMeasure3;
	}

	/**
	 * Permet de modifier la dose de l'ingrédient 3
	 * @param strMeasure3
	 */
	public void setStrMeasure3(String strMeasure3) {
		this.strMeasure3 = strMeasure3;
	}

	/**
	 * Permet d'obtenir la dose de l'ingrédient 4
	 * @return dose de l'ingrédient 4
	 */
	public String getStrMeasure4() {
		return strMeasure4;
	}

	/**
	 * Permet de modifier la dose de l'ingrédient 4
	 * @param strMeasure4
	 */
	public void setStrMeasure4(String strMeasure4) {
		this.strMeasure4 = strMeasure4;
	}

	/**
	 * Permet d'obtenir la dose de l'ingrédient 5
	 * @return dose de l'ingrédient 5
	 */
	public String getStrMeasure5() {
		return strMeasure5;
	}

	/**
	 * Permet de modifier la dose de l'ingrédient 5
	 * @param strMeasure5
	 */
	public void setStrMeasure5(String strMeasure5) {
		this.strMeasure5 = strMeasure5;
	}

	/**
	 * Permet d'obtenir la dose de l'ingrédient 6
	 * @return dose de l'ingrédient 6
	 */
	public String getStrMeasure6() {
		return strMeasure6;
	}

	/**
	 * Permet de modifier la dose de l'ingrédient 6
	 * @param strMeasure6
	 */
	public void setStrMeasure6(String strMeasure6) {
		this.strMeasure6 = strMeasure6;
	}

	/**
	 * Permet d'obtenir la dose de l'ingrédient 7
	 * @return dose de l'ingrédient 7
	 */
	public String getStrMeasure7() {
		return strMeasure7;
	}

	/**
	 * Permet de modifier la dose de l'ingrédient 7
	 * @param strMeasure7
	 */
	public void setStrMeasure7(String strMeasure7) {
		this.strMeasure7 = strMeasure7;
	}

	/**
	 * Permet d'obtenir la dose de l'ingrédient 8
	 * @return dose de l'ingrédient 8
	 */
	public String getStrMeasure8() {
		return strMeasure8;
	}

	/**
	 * Permet de modifier la dose de l'ingrédient 8
	 * @param strMeasure8
	 */
	public void setStrMeasure8(String strMeasure8) {
		this.strMeasure8 = strMeasure8;
	}

	/**
	 * Permet d'obtenir la dose de l'ingrédient 9
	 * @return dose de l'ingrédient 9
	 */
	public String getStrMeasure9() {
		return strMeasure9;
	}

	/**
	 * Permet de modifier la dose de l'ingrédient 9
	 * @param strMeasure9
	 */
	public void setStrMeasure9(String strMeasure9) {
		this.strMeasure9 = strMeasure9;
	}

	/**
	 * Permet d'obtenir la dose de l'ingrédient 10
	 * @return dose de l'ingrédient 10
	 */
	public String getStrMeasure10() {
		return strMeasure10;
	}

	/**
	 * Permet de modifier la dose de l'ingrédient 10
	 * @param strMeasure10
	 */
	public void setStrMeasure10(String strMeasure10) {
		this.strMeasure10 = strMeasure10;
	}

	/**
	 * Permet d'obtenir la dose de l'ingrédient 11
	 * @return dose de l'ingrédient 11
	 */
	public String getStrMeasure11() {
		return strMeasure11;
	}

	/**
	 * Permet de modifier la dose de l'ingrédient 11
	 * @param strMeasure11
	 */
	public void setStrMeasure11(String strMeasure11) {
		this.strMeasure11 = strMeasure11;
	}

	/**
	 * Permet d'obtenir la dose de l'ingrédient 12
	 * @return dose de l'ingrédient 12
	 */
	public String getStrMeasure12() {
		return strMeasure12;
	}

	/**
	 * Permet de modifier la dose de l'ingrédient 12
	 * @param strMeasure12
	 */
	public void setStrMeasure12(String strMeasure12) {
		this.strMeasure12 = strMeasure12;
	}

	/**
	 * Permet d'obtenir la dose de l'ingrédient 13
	 * @return dose de l'ingrédient 13
	 */
	public String getStrMeasure13() {
		return strMeasure13;
	}

	/**
	 * Permet de modifier la dose de l'ingrédient 13
	 * @param strMeasure13
	 */
	public void setStrMeasure13(String strMeasure13) {
		this.strMeasure13 = strMeasure13;
	}

	/**
	 * Permet d'obtenir la dose de l'ingrédient 14
	 * @return dose de l'ingrédient 14
	 */
	public String getStrMeasure14() {
		return strMeasure14;
	}

	/**
	 * Permet de modifier la dose de l'ingrédient 14
	 * @param strMeasure14
	 */
	public void setStrMeasure14(String strMeasure14) {
		this.strMeasure14 = strMeasure14;
	}

	/**
	 * Permet d'obtenir la dose de l'ingrédient 15
	 * @return dose de l'ingrédient 15
	 */
	public String getStrMeasure15() {
		return strMeasure15;
	}

	/**
	 * Permet de modifier la dose de l'ingrédient 15
	 * @param strMeasure15
	 */
	public void setStrMeasure15(String strMeasure15) {
		this.strMeasure15 = strMeasure15;
	}

	/**
	 * Permet d'obtenir la date de la dernière modification du drink
	 * @return date de la dernière modification du drink
	 */
	public String getDateModified() {
		return dateModified;
	}

	/**
	 * Permet de modifier la date de la dernière modification du drink
	 * @param dateModified
	 */
	public void setDateModified(String dateModified) {
		this.dateModified = dateModified;
	}
}
