package findYourCocktail.cocktail.drink.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import findYourCocktail.cocktail.drink.bean.Drink;

/**
 * Couche dao de DrinkService
 * @author Pirathina Jeyakumar
 *
 */
@Repository
public interface DrinkDaoService extends JpaRepository<Drink, String> {

}
