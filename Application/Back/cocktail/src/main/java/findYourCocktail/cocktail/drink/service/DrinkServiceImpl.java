package findYourCocktail.cocktail.drink.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import findYourCocktail.cocktail.apiExterne.service.ApiExterneService;
import findYourCocktail.cocktail.drink.bean.Drink;
import findYourCocktail.cocktail.exception.DrinkImplException;
import findYourCocktail.cocktail.exception.DrinkImplException.MessageDrinkException;

/**
 * Implémentation de DrinkService
 * @author Pirathina Jeyakumar
 *
 */
@Component
public class DrinkServiceImpl implements DrinkService {
	
	@Autowired
	private ApiExterneService apiExterneService;

	private static final String URL_DRINKS_PAR_ID = "https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=";
	private static final String URL_DRINKS_RANDOM = "https://www.thecocktaildb.com/api/json/v1/1/random.php";
	private static final String URL_DRINKS_PAR_CATEGORIE = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=";
	private static final String[] ALPHABET_NOMBRES = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
	private static final String KEY_DRINK = "drinks";
	private static final String CATEGORIE_ALCOHOLIC = "Alcoholic";
	private static final String CATEGORIE_NON_ALCOHOLIC = "Non alcoholic";
	private static final String CATEGORIE_OPTIONAL_ALCOHOL = "Optional alcohol";

	@Override
	public List<Drink> afficherTousLesDrinks() throws Exception{
		List<Drink> resultats = new ArrayList<>();

		for (int i = 0; i < ALPHABET_NOMBRES.length; i++) {			
			resultats.addAll(creationListeDrinks(apiExterneService.rechercheDrinkParLettreOuNom(ALPHABET_NOMBRES[i])));
		}
		return resultats;
	}

	@Override
	public List<Drink> afficherDrinksParPremiereLettre(String lettre) throws Exception {
		if(!StringUtils.isBlank(lettre) && lettre.length() == 1) {
			JSONArray jsonReponse = apiExterneService.rechercheDrinkParLettreOuNom(lettre);
			return creationListeDrinks(jsonReponse);
		} else {
			throw new DrinkImplException(MessageDrinkException.PARAMETREINCORRECT.getMessage());
		}
	}

	@Override
	public List<Drink> afficherDrinkParNom(String nom) throws Exception {
		if(!StringUtils.isBlank(nom) && nom.length() > 1) {
			JSONArray reponse = apiExterneService.rechercheDrinkParLettreOuNom(nom);
			return creationListeDrinks(reponse);
		} else {
			throw new DrinkImplException(MessageDrinkException.PARAMETREINCORRECT.getMessage());
		}
	}

	@Override
	public List<Drink> afficherDrinkParId(int id) throws Exception {
		if(id > 0) {
			JSONArray reponse = apiExterneService.rechercheDrinkParId(URL_DRINKS_PAR_ID, id);		
			return creationListeDrinks(reponse);			
		} else {
			throw new DrinkImplException(MessageDrinkException.PARAMETREINCORRECT.getMessage());
		}
	}

	@Override
	public List<Drink> afficherUnDrinkRandom() throws Exception {	
		JSONArray reponse = apiExterneService.rechercheDrinkRandom(URL_DRINKS_RANDOM);	
		return creationListeDrinks(reponse);
	}


	@Override
	public List<Drink> afficherDrinksParCategorie(String categorie) throws IOException, DrinkImplException {
		JSONObject data = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		String miseEnFormeParametre;
		
		if(!StringUtils.isBlank(categorie)) {
			miseEnFormeParametre = categorie.substring(0, 1).toUpperCase() + categorie.substring(1).toLowerCase();			
		} else {
			throw new DrinkImplException(MessageDrinkException.PARAMETREINCORRECT.getMessage());
		}

		if(miseEnFormeParametre.equals(CATEGORIE_ALCOHOLIC)) {
			data = apiExterneService.requeteHttpJsonObjectStringParametre(URL_DRINKS_PAR_CATEGORIE, CATEGORIE_ALCOHOLIC);
		} else if(miseEnFormeParametre.equals(CATEGORIE_NON_ALCOHOLIC)) {
			data = apiExterneService.requeteHttpJsonObjectStringParametre(URL_DRINKS_PAR_CATEGORIE, CATEGORIE_NON_ALCOHOLIC);
		} else if(miseEnFormeParametre.equals(CATEGORIE_OPTIONAL_ALCOHOL)) {
			data = apiExterneService.requeteHttpJsonObjectStringParametre(URL_DRINKS_PAR_CATEGORIE, CATEGORIE_OPTIONAL_ALCOHOL);
		} else {
			throw new DrinkImplException(MessageDrinkException.PARAMETREINCORRECT.getMessage());
		}

		if(!data.isNull(KEY_DRINK)) {
			jsonArray = data.getJSONArray(KEY_DRINK);
			return creationListeDrinkParCategorie(jsonArray);
		} else {
			throw new DrinkImplException(MessageDrinkException.REPONSEREQUETENULL.getMessage());
		}
	}

	/**
	 * Permet de créer une liste d'objets Drink à partir d'un array json
	 * @param jsonArray
	 * @return liste d'objets Drink
	 * @throws DrinkImplException
	 */
	private List<Drink> creationListeDrinks(JSONArray jsonArray) throws DrinkImplException{
		List<Drink> drinks = new ArrayList<>();

		if(!ObjectUtils.isEmpty(jsonArray)) {
			for(int i = 0; i < jsonArray.length(); i++) {

				String idDrink = jsonArray.getJSONObject(i).get("idDrink").toString();
				String strDrink = jsonArray.getJSONObject(i).get("strDrink").toString();
				String strCategory = jsonArray.getJSONObject(i).get("strCategory").toString();
				String strAlcoholic = jsonArray.getJSONObject(i).get("strAlcoholic").toString();
				String strGlass = jsonArray.getJSONObject(i).get("strGlass").toString();
				String strInstructions = jsonArray.getJSONObject(i).get("strInstructions").toString();
				String strDrinkThumb = jsonArray.getJSONObject(i).get("strDrinkThumb").toString();
				String strIngredient1 = jsonArray.getJSONObject(i).get("strIngredient1").toString();
				String strIngredient2 = jsonArray.getJSONObject(i).get("strIngredient2").toString();
				String strIngredient3 = jsonArray.getJSONObject(i).get("strIngredient3").toString();
				String strIngredient4 = jsonArray.getJSONObject(i).get("strIngredient4").toString();
				String strIngredient5 = jsonArray.getJSONObject(i).get("strIngredient5").toString();
				String strIngredient6 = jsonArray.getJSONObject(i).get("strIngredient6").toString();
				String strIngredient7 = jsonArray.getJSONObject(i).get("strIngredient7").toString();
				String strIngredient8 = jsonArray.getJSONObject(i).get("strIngredient8").toString();
				String strIngredient9 = jsonArray.getJSONObject(i).get("strIngredient9").toString();
				String strIngredient10 = jsonArray.getJSONObject(i).get("strIngredient10").toString();
				String strIngredient11 = jsonArray.getJSONObject(i).get("strIngredient11").toString();
				String strIngredient12 = jsonArray.getJSONObject(i).get("strIngredient12").toString();
				String strIngredient13 = jsonArray.getJSONObject(i).get("strIngredient13").toString();
				String strIngredient14 = jsonArray.getJSONObject(i).get("strIngredient14").toString();
				String strIngredient15 = jsonArray.getJSONObject(i).get("strIngredient15").toString();
				String strMeasure1 = jsonArray.getJSONObject(i).get("strMeasure1").toString();
				String strMeasure2 = jsonArray.getJSONObject(i).get("strMeasure2").toString();
				String strMeasure3 = jsonArray.getJSONObject(i).get("strMeasure3").toString();
				String strMeasure4 = jsonArray.getJSONObject(i).get("strMeasure4").toString();
				String strMeasure5 = jsonArray.getJSONObject(i).get("strMeasure5").toString();
				String strMeasure6 = jsonArray.getJSONObject(i).get("strMeasure6").toString();
				String strMeasure7 = jsonArray.getJSONObject(i).get("strMeasure7").toString();
				String strMeasure8 = jsonArray.getJSONObject(i).get("strMeasure8").toString();
				String strMeasure9 = jsonArray.getJSONObject(i).get("strMeasure9").toString();
				String strMeasure10 = jsonArray.getJSONObject(i).get("strMeasure10").toString();
				String strMeasure11 = jsonArray.getJSONObject(i).get("strMeasure11").toString();
				String strMeasure12 = jsonArray.getJSONObject(i).get("strMeasure12").toString();
				String strMeasure13 = jsonArray.getJSONObject(i).get("strMeasure13").toString();
				String strMeasure14 = jsonArray.getJSONObject(i).get("strMeasure14").toString();
				String strMeasure15 = jsonArray.getJSONObject(i).get("strMeasure15").toString();
				String dateModified = jsonArray.getJSONObject(i).get("dateModified").toString();

				Drink drink = new Drink(idDrink, strDrink, strCategory, strAlcoholic, strGlass, strInstructions, strDrinkThumb, strIngredient1, strIngredient2, strIngredient3, strIngredient4, strIngredient5, strIngredient6, strIngredient7, strIngredient8, strIngredient9, strIngredient10, strIngredient11, strIngredient12, strIngredient13, strIngredient14, strIngredient15, strMeasure1, strMeasure2, strMeasure3, strMeasure4, strMeasure5, strMeasure6, strMeasure7, strMeasure8, strMeasure9, strMeasure10, strMeasure11, strMeasure12, strMeasure13, strMeasure14, strMeasure15, dateModified);

				drinks.add(drink);
			}
		} else {
			throw new DrinkImplException(MessageDrinkException.LISTERETOURNEEVIDE.getMessage());
		}		
		return drinks;
	}

	/**
	 * Permet de créer une liste d'objets Drink  avec les attributs nom, id et image à partir d'un array json
	 * @param jsonArray
	 * @return liste d'objets Drink (nom, id et image)
	 */
	private List<Drink> creationListeDrinkParCategorie(JSONArray jsonArray){
		List<Drink> drinksParCategorie = new ArrayList<>();

		for(int i = 0; i < jsonArray.length(); i++) {
			String idDrink = jsonArray.getJSONObject(i).get("idDrink").toString();
			String strDrink = jsonArray.getJSONObject(i).get("strDrink").toString();
			String strDrinkThumb = jsonArray.getJSONObject(i).get("strDrinkThumb").toString();

			Drink drink = new Drink(idDrink, strDrink, strDrinkThumb);

			drinksParCategorie.add(drink);
		}
		return drinksParCategorie;
	}
}