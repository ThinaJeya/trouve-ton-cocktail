package findYourCocktail.cocktail.testDrinkService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import findYourCocktail.cocktail.drink.bean.Drink;
import findYourCocktail.cocktail.drink.service.DrinkService;
import findYourCocktail.cocktail.exception.DrinkImplException;
import findYourCocktail.cocktail.exception.DrinkImplException.MessageDrinkException;

/**
 * Classe test de DrinkService
 * @author Pirathina Jeyakumar
 *
 */
@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
class DrinkServiceTest {

	@Autowired
	private DrinkService drinkService;

	private static final String CATEGORIE_ALCOHOLIC = "Alcoholic";
	private static final String CATEGORIE_NON_ALCOHOLIC = "Non alcoholic";
	private static final String CATEGORIE_OPTIONAL_ALCOHOL = "Optional alcohol";

	/**
	 * Tests associés à la méthode afficherTousLesDrinks
	 * @throws Exception 
	 */
	@Test
	void afficherTousLesDrinksTest() throws Exception {		
		assertNotNull(afficherTousLesDrinksTest_casClassique());
	}

	private List<Drink> afficherTousLesDrinksTest_casClassique() throws Exception{
		return drinkService.afficherTousLesDrinks();
	}

	/**
	 * Tests associés à la méthode afficherDrinksParPremiereLettre
	 * @throws Exception
	 */
	@Test
	void afficherDrinksParPremiereLettreTest() throws Exception {
		Throwable parametreNull = assertThrows(DrinkImplException.class, () -> afficherDrinksParPremiereLettreTest_parametreNull());
		assertEquals(parametreNull.getMessage(), MessageDrinkException.PARAMETREINCORRECT.getMessage());

		Throwable parametreVide = assertThrows(DrinkImplException.class, () -> afficherDrinksParPremiereLettreTest_parametreVide());
		assertEquals(parametreVide.getMessage(), MessageDrinkException.PARAMETREINCORRECT.getMessage());

		Throwable tailleParametreIncorrecte = assertThrows(DrinkImplException.class, () -> afficherDrinksParPremiereLettreTest_tailleParametreIncorrecte());
		assertEquals(tailleParametreIncorrecte.getMessage(), MessageDrinkException.PARAMETREINCORRECT.getMessage());

		assertNotNull(afficherDrinksParPremiereLettreTest_casClassique());
	}


	private List<Drink> afficherDrinksParPremiereLettreTest_parametreNull() throws Exception{
		return drinkService.afficherDrinksParPremiereLettre(null);
	}

	private List<Drink> afficherDrinksParPremiereLettreTest_parametreVide() throws Exception{
		return drinkService.afficherDrinksParPremiereLettre(" ");
	}

	private List<Drink> afficherDrinksParPremiereLettreTest_tailleParametreIncorrecte() throws Exception{
		return drinkService.afficherDrinksParPremiereLettre("Kiwi");
	}


	private List<Drink> afficherDrinksParPremiereLettreTest_casClassique() throws Exception{
		return drinkService.afficherDrinksParPremiereLettre("a");
	}

	/**
	 * Tests associés à la méthode afficherDrinkParNomTest
	 * @throws Exception
	 */
	@Test
	void afficherDrinkParNomTest() throws Exception {
		Throwable parametreNull = assertThrows(DrinkImplException.class, () -> afficherDrinkParNomTest_parametreNull());
		assertEquals(parametreNull.getMessage(), MessageDrinkException.PARAMETREINCORRECT.getMessage());

		Throwable parametreVide = assertThrows(DrinkImplException.class, () -> afficherDrinkParNomTest_parametreVide());
		assertEquals(parametreVide.getMessage(), MessageDrinkException.PARAMETREINCORRECT.getMessage());

		Throwable tailleParametreIncorrecte = assertThrows(DrinkImplException.class, () -> afficherDrinkParNomTest_tailleParametreIncorrecte());
		assertEquals(tailleParametreIncorrecte.getMessage(), MessageDrinkException.PARAMETREINCORRECT.getMessage());

		assertNotNull(afficherDrinkParNomTest_casClassique());
	}

	private List<Drink> afficherDrinkParNomTest_parametreNull() throws Exception{
		return drinkService.afficherDrinkParNom(null);
	}

	private List<Drink> afficherDrinkParNomTest_parametreVide() throws Exception{
		return drinkService.afficherDrinkParNom(" ");
	}

	private List<Drink> afficherDrinkParNomTest_tailleParametreIncorrecte() throws Exception{
		return drinkService.afficherDrinkParNom("a");
	}

	private List<Drink> afficherDrinkParNomTest_casClassique() throws Exception{
		return drinkService.afficherDrinkParNom("mojito");
	}

	/**
	 * Tests associés à la méthode afficherDrinkParIdTest
	 * @throws Exception
	 */
	@Test
	void afficherDrinkParIdTest() throws Exception {
		Throwable parametreNegatif = assertThrows(DrinkImplException.class, () -> afficherDrinkParIdTest_parametreNegatif());
		assertEquals(parametreNegatif.getMessage(), MessageDrinkException.PARAMETREINCORRECT.getMessage());

		assertNotNull(afficherDrinkParIdTest_casClassique());
	}

	private List<Drink> afficherDrinkParIdTest_parametreNegatif() throws Exception{
		return drinkService.afficherDrinkParId(-15);
	}

	private List<Drink> afficherDrinkParIdTest_casClassique() throws Exception{
		return drinkService.afficherDrinkParId(15184);
	}

	/** 
	 * Tests associés à la méthode afficherUnDrinkRandomTest
	 * @throws Exception
	 */
	@Test
	void afficherUnDrinkRandomTest() throws Exception {
		assertNotNull(afficherUnDrinkRandomTest_casClassique());
	}

	private List<Drink> afficherUnDrinkRandomTest_casClassique() throws Exception{
		return drinkService.afficherUnDrinkRandom();
	}

	/**
	 * Tests associés à la méthode afficherDrinksParCategorieTest
	 * @throws IOException
	 * @throws DrinkImplException
	 */
	@Test
	void afficherDrinksParCategorieTest() throws IOException, DrinkImplException {
		Throwable parametreNull = assertThrows(DrinkImplException.class, () -> afficherDrinksParCategorieTest_parametreNull());
		assertEquals(parametreNull.getMessage(), MessageDrinkException.PARAMETREINCORRECT.getMessage());

		Throwable parametreVide = assertThrows(DrinkImplException.class, () -> afficherDrinksParCategorieTest_parametreVide());
		assertEquals(parametreVide.getMessage(), MessageDrinkException.PARAMETREINCORRECT.getMessage());

		assertNotNull(afficherDrinksParCategorieTest_categorieAlcoholic());
		assertNotNull(afficherDrinksParCategorieTest_categorieNonAlcoholic());
		assertNotNull(afficherDrinksParCategorieTest_categorieOptionalAlcoholic());

		Throwable categorieIncorrecte = assertThrows(DrinkImplException.class, () -> afficherDrinksParCategorieTest_categorieIncorrecte());
		assertEquals(categorieIncorrecte.getMessage(), MessageDrinkException.PARAMETREINCORRECT.getMessage());
	}

	private List<Drink> afficherDrinksParCategorieTest_parametreNull() throws IOException, DrinkImplException{
		return drinkService.afficherDrinksParCategorie(null);
	}

	private List<Drink> afficherDrinksParCategorieTest_parametreVide() throws IOException, DrinkImplException{
		return drinkService.afficherDrinksParCategorie(" ");
	}

	private List<Drink> afficherDrinksParCategorieTest_categorieAlcoholic() throws IOException, DrinkImplException{
		return drinkService.afficherDrinksParCategorie(CATEGORIE_ALCOHOLIC);
	}

	private List<Drink> afficherDrinksParCategorieTest_categorieNonAlcoholic() throws IOException, DrinkImplException{
		return drinkService.afficherDrinksParCategorie(CATEGORIE_NON_ALCOHOLIC);
	}

	private List<Drink> afficherDrinksParCategorieTest_categorieOptionalAlcoholic() throws IOException, DrinkImplException{
		return drinkService.afficherDrinksParCategorie(CATEGORIE_OPTIONAL_ALCOHOL);
	}

	private List<Drink> afficherDrinksParCategorieTest_categorieIncorrecte() throws IOException, DrinkImplException{
		return drinkService.afficherDrinksParCategorie("Alcool");
	}
}
