package findYourCocktail.cocktail.testApiExterneService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import findYourCocktail.cocktail.apiExterne.service.ApiExterneService;
import findYourCocktail.cocktail.exception.DrinkImplException;
import findYourCocktail.cocktail.exception.DrinkImplException.MessageDrinkException;

@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
class ApiExterneServiceTest {

	@Autowired
	private ApiExterneService apiExterneService;

	private static final String URL_DRINKS_PAR_NOM = "https://www.thecocktaildb.com/api/json/v1/1/search.php?s=";

	/**
	 * Tests associés à la méthode rechercheDrinkParLettreOuNom
	 */
	@Test
	void rechercheDrinkParLettreOuNomTest() {
		Throwable parametreStringNull = assertThrows(DrinkImplException.class, () -> rechercheDrinkParLettreOuNomTest_parametreNull());
		assertEquals(parametreStringNull.getMessage(), MessageDrinkException.PARAMETREMETHODEINTERNEINCORRECT.getMessage());
	}

	private JSONArray rechercheDrinkParLettreOuNomTest_parametreNull() throws IOException, DrinkImplException {
		return apiExterneService.rechercheDrinkParLettreOuNom(null);
	}

	/**
	 * Tests associés à la méthode requeteHttpJsonObjectStringParametre
	 */
	@Test
	void requeteHttpJsonObjectStringParametreTest() {
		Throwable parametreStringNull = assertThrows(DrinkImplException.class, () -> requeteHttpJsonObjectStringParametreTest_parametreNull());
		assertEquals(parametreStringNull.getMessage(), MessageDrinkException.PARAMETREMETHODEINTERNEINCORRECT.getMessage());
	}

	private JSONObject requeteHttpJsonObjectStringParametreTest_parametreNull() throws IOException, DrinkImplException {
		return apiExterneService.requeteHttpJsonObjectStringParametre(null, null);
	}

	/**
	 * Tests associés à la méthode rechercheDrinkParId
	 */
	@Test
	void rechercheDrinkParIdTest() {
		Throwable parametreStringNull = assertThrows(DrinkImplException.class, () -> rechercheDrinkParIdTest_parametreStringNull());
		assertEquals(parametreStringNull.getMessage(), MessageDrinkException.PARAMETREMETHODEINTERNEINCORRECT.getMessage());

		Throwable parametreIntIncorrect = assertThrows(DrinkImplException.class, () -> rechercheDrinkParIdTest_parametreIntIncorrect());
		assertEquals(parametreIntIncorrect.getMessage(), MessageDrinkException.PARAMETREMETHODEINTERNEINCORRECT.getMessage());
	}

	private JSONArray rechercheDrinkParIdTest_parametreStringNull() throws DrinkImplException, IOException {
		return apiExterneService.rechercheDrinkParId(null, 1523);
	}

	private JSONArray rechercheDrinkParIdTest_parametreIntIncorrect() throws DrinkImplException, IOException {
		return apiExterneService.rechercheDrinkParId(URL_DRINKS_PAR_NOM, -56);
	}

	/**
	 * Tests associés à la méthode rechercheDrinkRandom
	 */
	@Test
	void rechercheDrinkRandomTest() {
		Throwable parametreVide = assertThrows(DrinkImplException.class, () -> rechercheDrinkRandomTest_parametreVide());
		assertEquals(parametreVide.getMessage(), MessageDrinkException.PARAMETREMETHODEINTERNEINCORRECT.getMessage());
	}

	private JSONArray rechercheDrinkRandomTest_parametreVide() throws IOException, DrinkImplException {
		return apiExterneService.rechercheDrinkRandom(" ");
	}
}
