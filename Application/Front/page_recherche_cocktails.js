document.addEventListener("DOMContentLoaded", function () {
  searchResultsDisplay();
})

// Déclaration des url en constantes
const URL_TOUS_LES_DRINKS = 'http://localhost:8081/drinks/tousLesDrinks/';
const URL_DRINKS_PAR_LETTRE = 'http://localhost:8081/drinks/drinksParPremiereLettre/';
const URL_DRINKS_PAR_NOM = 'http://localhost:8081/drinks/drinksParNom/';
const URL_DRINKS_PAR_CATEGORIE = 'http://localhost:8081/drinks/drinksParCategorie/';

const ALLDRINKS = "all-drinks";
const ALCOHOLDRINKS = "Alcoholic";
const VIRGINDRINKS = "Non alcoholic";
const OPTIONALDRINKS = "Optional alcohol";

const ERRORMESSAGE = "Erreur dans la requête ajax ! ";

/**
 * Permet d'orienter la requête suivant le type de paramètre associé à la requête formulée
 */
function searchResultsDisplay() {
  let searchParameter = getSearchParameterFromUrl();
  let longueurSearchParameter = searchParameter.length;

  if (longueurSearchParameter === 1) {
    ajaxCall(URL_DRINKS_PAR_LETTRE, searchParameter);
  } else if (longueurSearchParameter > 1 && searchParameter !== ALLDRINKS && searchParameter !== VIRGINDRINKS && searchParameter !== OPTIONALDRINKS && searchParameter !== ALCOHOLDRINKS) {
    ajaxCall(URL_DRINKS_PAR_NOM, searchParameter);
  } else if (longueurSearchParameter > 1 && searchParameter === ALLDRINKS) {
    ajaxCall(URL_TOUS_LES_DRINKS, "");
  } else if (longueurSearchParameter > 1 && searchParameter === VIRGINDRINKS) {
    ajaxCall(URL_DRINKS_PAR_CATEGORIE, searchParameter);
  } else if (longueurSearchParameter > 1 && searchParameter === OPTIONALDRINKS) {
    ajaxCall(URL_DRINKS_PAR_CATEGORIE, searchParameter);
  } else if (longueurSearchParameter > 1 && searchParameter === ALCOHOLDRINKS) {
    ajaxCall(URL_DRINKS_PAR_CATEGORIE, searchParameter);
  }
}

/**
 * Requête ajax vers le back-end permettant de récupérer les informations de cocktail
 * @param {string} url 
 * @param {string} searchParameter 
 */
function ajaxCall(url, searchParameter) {
  let divResultats = document.getElementById('sous-bloc-resultats');

  $.ajax({
    url: url + searchParameter,
    type: 'GET',
    dataType: 'JSON',
    contentType: "application/json",
    success: function (data, status) {
      displaySearchDrinkHTML(data, divResultats, searchParameter);
    },
    error: function (erreur) {
      console.log(ERRORMESSAGE + erreur.status);
    }
  });
}

/**
 * Affichage des résultats de la recherche avec titre et image pour chaque cocktail
 * @param {JSON} data 
 * @param {string} htmlIdDiv 
 * @param {string} searchParameter 
 */
function displaySearchDrinkHTML(data, htmlIdDiv, searchParameter) {
  let titleDiv = document.getElementById('titre-resultats');
  titleDiv.innerHTML ="";
  if (data !== null || data !== undefined) {
    if (searchParameter !== "") {
      $(titleDiv).append(`<h2>${data.length} Cocktail results for "${searchParameter}"</h2>`);
    } else {
      $(titleDiv).append(`<h2>${data.length} Cocktail results</h2>`);
    }

    for (let i = 0; i < data.length; i++) {
      $(htmlIdDiv).append(
        `<div class="col-lg-4 col-xl-4 col-md-12 col-sm-12 search-cocktail-div">
        <a class="item-over" id="${data[i].idDrink}" onclick="afficherCocktail(this.id)">
                  <div class="cocktail-picture" id="${data[i].idDrink}">
                      <img class="drink-picture" src="${data[i].strDrinkThumb}" id="search-cocktail-picture-${data[i].idDrink}">
                  </div>
                  <h4 class="cocktail-title" id="search-cocktail-title-${data[i].idDrink}">${data[i].strDrink}</h4>
              </a></div>`);
    }
  } else {
    $(titleDiv).append(`No results to display!`);
  }
}

/**
 * Permet de récupérer le contenu du paramètre search de l'url
 */
function getSearchParameterFromUrl() {
  let url_string = window.location.href;
  let url = new URL(url_string);
  let searchParameter = url.searchParams.get("search");
  return searchParameter;
}

function afficherCocktail(id) {
  window.location.href = "./page_cocktail.html?id=" + id;
}