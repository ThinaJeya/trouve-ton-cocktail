document.addEventListener("DOMContentLoaded", function () {
    headerDisplay();
    footerDisplay();
});

/**
 * Affichage du header
 */
function headerDisplay() {
    const HEADER = document.querySelector("header");

    fetch("./header.html")
        .then(response => {
            return response.text();
        })
        .then(data => {
            HEADER.innerHTML = data;
        });
}

/**
 * Affichage du footer
 */
function footerDisplay() {
    const FOOTER = document.querySelector("footer");

    fetch("./footer.html")
        .then(response => {
            return response.text();
        })
        .then(data => {
            FOOTER.innerHTML = data;
        });
}

/**
 * Choix d'un random cocktail et redirection vers la page_cocktail quand clic sur le "find me random cocktail" du header de la page (onclick)
 */
function displayHeaderRandomDrink() {
    $.ajax({
        url: 'http://localhost:8081/drinks/drinkRandom',
        type: 'GET',
        dataType: 'JSON',
        contentType: "application/json",
        success: function (data, status) {
            for (let i = 0; i < data.length; i++) {
                afficherCocktail(data[i].idDrink);
            }
        },
        error: function (erreur) {
            console.log(ERRORMESSAGE + erreur.status);
        }
    });
}

/**
 * Redirection vers la page d'affichage des résultats de cocktails en prenant en paramètre l'id
 * @param {string} id 
 */
function afficherDrinksDepuisHeaderFooter(id) {
    window.location.href = "./page_recherche_cocktails.html?search=" + id;
}