document.addEventListener("DOMContentLoaded", function () {

    const RANDOM = "random";
    const ID = "id";

    ajaxRequestDrink(ID, "", "178336", "#cocktail-div-1", "#selection-cocktail-title-1", "#selection-cocktail-picture-1");
    ajaxRequestDrink(ID, "", "17215", "#cocktail-div-2", "#selection-cocktail-title-2", "#selection-cocktail-picture-2");
    ajaxRequestDrink(ID, "", "11001", "#cocktail-div-3", "#selection-cocktail-title-3", "#selection-cocktail-picture-3");
    ajaxRequestDrink(ID, "", "178312", "#cocktail-div-4", "#selection-cocktail-title-4", "#selection-cocktail-picture-4");

    ajaxRequestDrink(RANDOM, "", "", "#cocktail-div-5", "#random-cocktail-title-1", "#random-cocktail-picture-1");
    ajaxRequestDrink(RANDOM, "", "", "#cocktail-div-6", "#random-cocktail-title-2", "#random-cocktail-picture-2");
    ajaxRequestDrink(RANDOM, "", "", "#cocktail-div-7", "#random-cocktail-title-3", "#random-cocktail-picture-3");
    ajaxRequestDrink(RANDOM, "", "", "#cocktail-div-8", "#random-cocktail-title-4", "#random-cocktail-picture-4");

    submitFormulaireRecherche();
});

const URL_TOUS_LES_DRINKS = 'http://localhost:8081/drinks/tousLesDrinks/';
const URL_DRINK_RANDOM = 'http://localhost:8081/drinks/drinkRandom';
const URL_DRINKS_PAR_NOM = 'http://localhost:8081/drinks/drinksParNom/';
const URL_DRINKS_PAR_ID = 'http://localhost:8081/drinks/drinkParId/';

/**
 * Requête pour récupérer un cocktail suivant le type de requete : random, recherche par nom ou par id
 * @param {string} request 
 * @param {string} strDrink 
 * @param {string} idDrink 
 * @param {string} htmlIdDiv 
 * @param {string} htmlIdTitle 
 * @param {string} htmlIdPicture 
 */
function ajaxRequestDrink(request, strDrink, idDrink, htmlIdDiv, htmlIdTitle, htmlIdPicture) {
    if (request === "random") {
        ajaxCallPageAccueil(URL_DRINK_RANDOM, "", htmlIdDiv, htmlIdTitle, htmlIdPicture);
    } else if (request === "name") {
        ajaxCallPageAccueil(URL_DRINKS_PAR_NOM, strDrink, htmlIdDiv, htmlIdTitle, htmlIdPicture);
    } else if (request === "id") {
        ajaxCallPageAccueil(URL_DRINKS_PAR_ID, idDrink, htmlIdDiv, htmlIdTitle, htmlIdPicture);
    }
}

/**
 * Méthode pour la requête ajax vers le back-end permettant de récupérer les informations de cocktail
 * @param {string} url 
 * @param {string} searchParameter 
 * @param {string} htmlIdDiv 
 * @param {string} htmlIdTitle 
 * @param {string} htmlIdPicture 
 */
function ajaxCallPageAccueil(url, searchParameter, htmlIdDiv, htmlIdTitle, htmlIdPicture) {
    const ERRORMESSAGE = "Erreur dans la requête AJAX !";

    $.ajax({
        url: url + searchParameter,
        type: 'GET',
        dataType: 'JSON',
        contentType: "application/json",
        success: function (data, status) {
            displayDrinkHTML(data, htmlIdDiv, htmlIdTitle, htmlIdPicture);
        },
        error: function (erreur) {
            console.log(ERRORMESSAGE + erreur.status);
        }
    });
}

/**
 * Affichage du cocktail avec son titre et son image dans les rubriques selection/random
 * @param {JSON} data 
 * @param {*} htmlIdDiv 
 * @param {*} htmlIdTitle 
 * @param {*} htmlIdPicture 
 */
function displayDrinkHTML(data, htmlIdDiv, htmlIdTitle, htmlIdPicture) {
    htmlIdDiv.innerHTML = "";

    if (data !== null || data !== undefined) {
        for (let i = 0; i < data.length; i++) {
            $(htmlIdDiv).append(
                `<a class="item-over" id="${data[i].idDrink}" onclick="afficherCocktail(this.id)">
                    <div class="cocktail-picture" id="${htmlIdPicture}">
                        <img class="drink-picture" src="${data[i].strDrinkThumb}" id="${htmlIdPicture}-${data[i].idDrink}"/>
                    </div>
                    <h3 class="cocktail-title" id="${htmlIdTitle}">${data[i].strDrink}</h3>
                </a>`);
        }
    }
}

/**
 * Redirection vers la page de description du cocktail en prenant l'id comme paramètre
 * @param {number} id 
 */
function afficherCocktail(id) {
    window.location.href = "./page_cocktail.html?id=" + id;
}

/**
 * Validation du formulaire de recherche et redirection vers la page de résultat de la recherche
 */
function submitFormulaireRecherche() {
    let formulaireRecherche = document.getElementById('search-form');

    formulaireRecherche.addEventListener('submit', function(event){
        event.preventDefault();
        let valeurRecherche = document.getElementById('search-input').value;

        redirectionPageCocktail(valeurRecherche);
    });
}

function redirectionPageCocktail(parametre){
    const urlPageCocktail = './page_recherche_cocktails.html?search=';
    window.location.href = urlPageCocktail + parametre;
} 